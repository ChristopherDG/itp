<?php


namespace Gamma\Dogs\Api\Data;


interface BookInterface
{
    const TITLE = 'title';
    const ID = 'id';
    const AUTHOR = 'author';
    const IMAGE = 'image';
    const LINK = 'link';

    public function getTitle(): string;

    public function setTitle(string $title): BookInterface;

    public function getId(): string ;

    public function setId(string $id): BookInterface;

    public function getImage(): string;

    public function setImage(string $image): BookInterface;

    public function getAuthor(): string;

    public function setAuthor(string $author): BookInterface;

    public function getLink(): string;

    public function setLink(string $link): BookInterface;
}