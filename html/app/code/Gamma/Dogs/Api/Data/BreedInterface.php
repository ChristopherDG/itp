<?php


namespace Gamma\Dogs\Api\Data;


interface BreedInterface
{
    const NAME = 'name';
    const SUB_BREEDS = 'sub-breeds';
    const IMAGE = 'image';
    const BOOKS = 'books';

    public function getName(): string;

    public function setName(string $name): BreedInterface;

    public function getSubBreeds(): array ;

    public function setSubBreeds(array $sub_Breeds): BreedInterface;

    public function getImage(): string;

    public function setImage(string $image): BreedInterface;

    public function getBooks(): array ;

    public function setBooks(array $books): BreedInterface;
}