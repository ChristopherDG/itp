<?php


namespace Gamma\Dogs\Api;


interface ConnectionInterface
{
    public function getInfo($path): array;
}