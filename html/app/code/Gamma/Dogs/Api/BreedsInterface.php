<?php


namespace Gamma\Dogs\Api;

use Gamma\Dogs\Api\Data\BreedInterface;


interface BreedsInterface
{
    public function getBreedsList(): array;

    public function getBreed(string $name): BreedInterface;

}