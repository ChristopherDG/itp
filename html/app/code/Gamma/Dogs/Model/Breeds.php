<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\BookConnectionInterface;
use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\BreedsInterface;
use Gamma\Dogs\Api\Data\BreedInterfaceFactory;
use Gamma\Dogs\Api\Data\BreedInterface;
use Gamma\Dogs\Api\Data\BookInterfaceFactory;

class Breeds implements BreedsInterface
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var BookConnection
     */
    protected $bookConnection;
    /**
     * @var BreedInterfaceFactory
     */
    protected $breedFactory;

    /**
     * @var BookInterfaceFactory
     */
    protected $bookFactory;

    public function __construct(
        ConnectionInterface $connection,
        BookConnectionInterface $bookConnection,
        BreedInterfaceFactory $breedFactory,
        BookInterfaceFactory $bookFactory
    ){
      $this->connection = $connection;
      $this->bookConnection = $bookConnection;
      $this->breedFactory = $breedFactory;
      $this->bookFactory = $bookFactory;
    }

    public function getBreedsList(): array
    {
        $breedsData = $this->connection->getInfo('breeds/list/all');

        return $this->processBreedsList($breedsData);

    }

    public function  processBreedsList(array $breedsData){
        return array_keys($breedsData['message']);
    }

    public function getBreed(string $name): BreedInterface{
        $breed = $this->breedFactory->create();

        $breedImage = $this->processImage($name);
        $breedSubBreeds = $this->processSubBreeds($name);
        $breedBooks = $this->processBooks($name);

        $breed->setImage($breedImage)
            ->setName(ucfirst($name))
            ->setSubBreeds($breedSubBreeds)
            ->setBooks($breedBooks);

        return $breed;
    }

    public function processSubBreeds(string $name){
        $breedsData = $this->connection->getInfo('breeds/list/all');
        return $breedsData['message'][$name];
    }

    public function processImage(string $name){
        $breedsData = $this->connection->getInfo("breed/{$name}/images");
        return $breedsData['message'][0];
    }

    public function processBooks(string $name){
        $booksData = $this->bookConnection->getBooks(ucfirst($name));

        $books = Array();
        $count = 1;
        foreach ($booksData['search']['results']['work'] as $work){
            if($count <= 3){
                $bookInfo = $work['best_book'];
                $bookIns = $this->bookFactory->create();
                $bookIns->setId($bookInfo['id'])
                    ->setTitle($bookInfo['title'])
                    ->setAuthor($bookInfo['author']['name'])
                    ->setImage($bookInfo['image_url']);
                array_push($books, $bookIns);
                $count++;
            }
        }

        foreach ($books as $book){
            $bookInfo = $this->bookConnection->getBookInfo($book->getId())['book'];
            $book->setLink($bookInfo['url']);
        }

        return $books;
    }

}

/**
 *
 */