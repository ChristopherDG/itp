<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\BookConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

class BookConnection implements BookConnectionInterface
{
    protected $baseUrl;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://www.goodreads.com'
    )
    {
        $this->baseUrl = $baseUrl;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
    }

    public function getBooks($breed): array
    {
        $requestPath = "{$this->baseUrl}/search/index.xml?key=Hij5lvX8Ia0MJq4adc5Lg&q={$breed}+Dog";
        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        $xml = simplexml_load_string($response);
        $array = json_decode(json_encode($xml), true);

        return $array;
    }

    public function getBookInfo($id): array
    {
        $requestPath = "{$this->baseUrl}/book/show/{$id}.xml?key=Hij5lvX8Ia0MJq4adc5Lg";
        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        $xml = simplexml_load_string($response, null
            , LIBXML_NOCDATA);
        $array = json_decode(json_encode($xml), true);

        return $array;
    }
}