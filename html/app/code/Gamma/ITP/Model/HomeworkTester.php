<?php


namespace Gamma\ITP\Model;


use Exception;
use Gamma\ITP\Api\Data\TestCaseInterface;
use Gamma\ITP\Api\HomeworkTesterInterface;

class HomeworkTester implements HomeworkTesterInterface
{
    /**
     * Runs the test cases on a given homework
     * @param $homework Object A PHP class that will be evaluated
     * @param TestCaseInterface[] $testCases
     * @return TestCaseInterface[] The test cases with the populated result field
     */
    public function run($homework, array $testCases): array
    {
        $testCases = $this->runAllCases($homework, $testCases);

        foreach ($testCases as $testCase) {
            $this->displayResults($testCase);
        }

        return $testCases;
    }

    /**
     * Runs all the given test cases, passing or failing them as needed
     *
     * @param $homework
     * @param array $testCases
     * @return TestCaseInterface[]
     */
    protected function runAllCases($homework, array $testCases): array
    {
        foreach ($testCases as $testCase) {
            try {
                $this->runTestCase($homework, $testCase);
            } catch (Exception $exception) {
                $this->handleTestException($testCase, $exception);
            }
        }

        return $testCases;
    }

    /**
     * Runs a single test case on a given homework
     *
     * @param $homework
     * @param $testCase
     */
    protected function runTestCase($homework, TestCaseInterface &$testCase)
    {
        $functionName = $testCase->getFunctionName();
        $result = $homework->$functionName(...$testCase->getArguments());

        if(is_array($result)) {
            $testCase->setPassed(array_values($result) == $testCase->getExpected())
                ->setResult($result);
        } else {
            $testCase->setPassed($result === $testCase->getExpected())
                ->setResult($result);
        }
    }

    /**
     * Records an exception as the test case result, setting its passed to false
     *
     * @param TestCaseInterface $testCase
     * @param Exception $exception
     */
    protected function handleTestException(TestCaseInterface &$testCase, Exception $exception)
    {
        $testCase->setPassed(false)
            ->setResult($exception);
    }

    public function displayResults(TestCaseInterface $testCase): void
    {
        $args = json_encode($testCase->getArguments());
        echo "Test case: {$testCase->getFunctionName()}({$args}): " . ($testCase->getPassed() ? 'PASSED' : 'FAILED') . "\n";
    }
}